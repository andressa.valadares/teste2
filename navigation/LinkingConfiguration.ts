import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Root: {
        screens: {
          Inventario: {
            screens: {
              InventarioScreen: 'inventario',
            },
          },
          Config: {
            screens: {
              ConfigScreen: 'config',
            },
          },
        },
      },
      NotFound: '*',
    },
  },
};
