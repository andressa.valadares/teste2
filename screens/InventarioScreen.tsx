import * as React from 'react';
import { StyleSheet } from 'react-native';

import InventarioList from '../components/InventarioList';
import { Text, View } from '../components/Themed';

export default function InventarioScreen() {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Inventários</Text>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <InventarioList path="/screens/InventarioScreen.tsx" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
