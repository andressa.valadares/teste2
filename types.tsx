export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  Inventario: undefined;
  Config: undefined;
};

export type InventarioParamList = {
  InventarioScreen: undefined;
};

export type ConfigParamList = {
  ConfigScreen: undefined;
};
